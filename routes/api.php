<?php

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ReportController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\ReporterController;
use App\Http\Controllers\API\ReportTrackerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/reporters', [ReporterController::class, 'store'])->name('reports.store');
Route::post('/reports', [ReportController::class, 'store'])->name('reports.store');
Route::post('/login', [AuthController::class, 'login'])->name('auth.login');

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/categories', [CategoryController::class, 'index'])->name('categories.index');
    Route::get('/categories/json', [CategoryController::class, 'indexToJson'])->name('categories.index.json');
    Route::get('/categories/{category}', [CategoryController::class, 'show'])->name('categories.show');
    Route::post('/categories', [CategoryController::class, 'store'])->name('categories.store');
    Route::post('/categories/{category}', [CategoryController::class, 'update'])->name('categories.update');
    Route::delete('/categories/{category}', [CategoryController::class, 'destroy'])->name('categories.destroy');

    Route::get('/reports', [ReportController::class, 'index'])->name('reports.index');
    Route::get('/reports/json', [ReportController::class, 'indexToJson'])->name('reports.index.json');
    Route::get('/reports/{report}', [ReportController::class, 'show'])->name('reports.show');

    Route::get('/reporters', [ReporterController::class, 'index'])->name('reports.index');
    Route::get('/reporters/json', [ReporterController::class, 'indexToJson'])->name('reports.index.json');
    Route::post('/reporters/{reporter}', [ReporterController::class, 'update'])->name('reports.update');
    Route::get('/reporters/{reporter}', [ReporterController::class, 'show'])->name('reports.show');

    Route::get('/report-trackers', [ReportTrackerController::class, 'index'])->name('report-trackers.index');

    Route::post('/reports/{report}/verify', [ReportController::class, 'verify'])->name('reports.verify');

    Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');
    Route::get('/me', [AuthController::class, 'me'])->name('auth.me');
});
