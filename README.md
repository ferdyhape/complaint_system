# Complaint System App

## Table of Contents

1. [Creator](#creator)
2. [Description](#description)
3. [Technology](#technology)
4. [Feature](#feature)
5. [How To Use](#how-to-use)
6. [About Creator](#about-creator)


## Creator
Name: ``` Ferdy Hahan Pradana ```

## Description
- This repository was created to fulfill the task of creating a complaint system application for internship students at PT Adma Digital Solusi.

## Technology
- Laravel 10
- Vue 3
- MySQL
- Bootstrap 5
- Jquery
- Fontawesome
- Axios
- DataTables (datatables.net)

## API Route
Note: This app is using full API, so the view is using Vue 3
<table>
    <thead>
        <tr>
            <th>API</th>
            <th>Information</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>/reporters</td>
            <td>Store a reporter</td>
        </tr>
        <tr>
            <td>/reports</td>
            <td>Store a report</td>
        </tr>
        <tr>
            <td>/login</td>
            <td>Login</td>
        </tr>
        <tr>
            <td>/categories</td>
            <td>List all categories</td>
        </tr>
        <tr>
            <td>/categories/json</td>
            <td>List categories in JSON format</td>
        </tr>
        <tr>
            <td>/categories/{category}</td>
            <td>Show a specific category</td>
        </tr>
        <tr>
            <td>/categories</td>
            <td>Create a new category</td>
        </tr>
        <tr>
            <td>/categories/{category}</td>
            <td>Update a category</td>
        </tr>
        <tr>
            <td>/categories/{category}</td>
            <td>Delete a category</td>
        </tr>
        <tr>
            <td>/reports</td>
            <td>List all reports</td>
        </tr>
        <tr>
            <td>/reports/json</td>
            <td>List reports in JSON format</td>
        </tr>
        <tr>
            <td>/reports/{report}</td>
            <td>Show a specific report</td>
        </tr>
        <tr>
            <td>/reporters</td>
            <td>List all reporters</td>
        </tr>
        <tr>
            <td>/reporters/json</td>
            <td>List reporters in JSON format</td>
        </tr>
        <tr>
            <td>/reporters/{reporter}</td>
            <td>Update a reporter</td>
        </tr>
        <tr>
            <td>/reporters/{reporter}</td>
            <td>Show a specific reporter</td>
        </tr>
        <tr>
            <td>/report-trackers</td>
            <td>List report trackers</td>
        </tr>
        <tr>
            <td>/reports/{report}/verify</td>
            <td>Verify a report</td>
        </tr>
        <tr>
            <td>/logout</td>
            <td>Logout</td>
        </tr>
        <tr>
            <td>/me</td>
            <td>Get current user information</td>
        </tr>
    </tbody>
</table>

## Feature 
1.  Authentication (admin)
2.  Report Management (admin)
3.  Category Management (admin)
4.  Reporter Management (admin)
5.  List Report Tracker (admin)
3.  Create Report (user)


## Preview Capture App Link
- You can see [in here](https://drive.google.com/file/d/1TLvNAiYIRQzVWn4j-Fmdj5N_dZubAXOI/view?usp=sharing)


## How To Use
1.  Clone this repository
    ```
    https://gitlab.com/ferdyhape/complaint_system.git
    ```
2.  Copy paste **.env.example** file and rename as **.env**
3.  Adjust the database name in the env file on **DB_DATABASE**

3.  Generate Key
    ```
    php artisan key:generate
    ```
4.  Install dependencies
    ```
    composer install
    npm install
    ```
5.  Generate mirror link
    ```
    php artisan storage:link
    ```
    
6.  Migrate the tables
    ```
    php artisan migrate
    ```

7.  Insert the data from seeder to database
    ```
    php artisan db:seed
    ```

8.  Start the server PHP
    ```
    php artisan serve
    ```

9. Start the server Node
    ```
    npm run dev
    ```
9.  Login with this crediential

    * username: <br><br>
      ```
      ferdyhape
      ```
    * Password: <br><br>
      ```
      password
      ```
    
11. Enjoy use!

### About Creator
[![portfolio](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://www.ferdyhape.site/)
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/ferdy-hahan-pradana)
[![instagram](https://img.shields.io/badge/instagram-833AB4?style=for-the-badge&logo=instagram&logoColor=white)](https://instagram.com/ferdyhape)
[![github](https://img.shields.io/badge/github-333?style=for-the-badge&logo=github&logoColor=white)](https://github.com/ferdyhape)
