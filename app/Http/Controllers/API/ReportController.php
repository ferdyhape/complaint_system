<?php

namespace App\Http\Controllers\API;

use App\Models\Report;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Report\CreateReport;
use App\Http\Requests\Report\VerifyRequest;
use App\Http\Resources\Report\ReportIndexResource;
use App\Http\Resources\Report\ReportSpecificResource;
use Yajra\DataTables\Facades\DataTables;

class ReportController extends Controller
{

    public function index(Request $request)
    {
        try {
            $reporters = Report::with(['reporter', 'category'])->get();
            return Datatables::of($reporters)
                ->addIndexColumn()
                ->make(true);
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }

    public function indexToJson()
    {
        try {
            $reporters = Report::all();
            return ResponseFormatter::success(
                ReportIndexResource::collection($reporters),
                'Data retrieved successfully'
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }

    public function show(Report $report)
    {
        try {
            return ResponseFormatter::success(
                new ReportSpecificResource($report),
                'Data retrieved successfully'
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }

    public function store(CreateReport $request)
    {
        try {
            $data = $request->validated();
            $report = Report::create($data);

            if ($request->hasFile('image')) {
                // $report->addMedia($request->file('image'))
                //     ->usingName($report->ticket_id)
                //     ->usingFileName('image-' . $report->ticket_id . '.' . $request->file('image')->getClientOriginalExtension())
                //     ->toMediaCollection('reports');

                foreach ($request->file('image') as $image) {
                    $report->addMedia($image)
                        ->usingName($report->ticket_id)
                        ->usingFileName('image-' . $report->ticket_id . '.' . $image->getClientOriginalExtension())
                        ->toMediaCollection('reports');
                }
            }

            return ResponseFormatter::success(
                new ReportSpecificResource($report),
                'Data created successfully'
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }

    public function verify(VerifyRequest $request, Report $report)
    {
        // dd($request->validated());
        try {
            $data = $request->validated();

            $report->update($data);
            if (isset($data['status'])) {
                $report->reportTrackers()->create([
                    'user_id' => 1, // user id ini masih statis karena belum ada login
                    'report_id' => $report->id,
                    'status' => $data['status'],
                    'note' => $data['note'] ?? '',
                ]);
            }
            return ResponseFormatter::success(
                new ReportSpecificResource($report),
                'Data updated successfully'
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }
}
