<?php

namespace App\Http\Controllers\API;

use App\Models\Report;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\ReportTracker;
use Yajra\DataTables\Facades\DataTables;

class ReportTrackerController extends Controller
{
    public function index(Request $request)
    {
        try {
            $reporttrackers = ReportTracker::with(['report', 'user'])->get();
            return Datatables::of($reporttrackers)
                ->addIndexColumn()
                ->make(true);
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }

    public function indexToJson()
    {
        try {
            $reporttrackers = ReportTracker::all();
            return ResponseFormatter::success(
                $reporttrackers,
                'Data retrieved successfully'
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }
}
