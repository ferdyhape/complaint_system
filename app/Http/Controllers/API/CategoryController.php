<?php

namespace App\Http\Controllers\API;

use Log;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CreateCategory;
use App\Http\Requests\Category\UpdateCategory;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Resources\Category\CategoryResource;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        try {
            $categories = Category::all();
            return Datatables::of($categories)
                ->addIndexColumn()
                ->make(true);
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }

    public function indexToJson()
    {
        try {
            $categories = Category::all();
            return ResponseFormatter::success(
                CategoryResource::collection($categories),
                'Categories data retrieved successfully',
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }

    public function show(Category $category)
    {
        try {
            return ResponseFormatter::success(
                new CategoryResource($category),
                'Category data retrieved successfully',
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }

    public function store(CreateCategory $request)
    {
        try {
            $data = $request->validated();
            $category = Category::create($data);
            return ResponseFormatter::success(
                new CategoryResource($category),
                'Category created successfully',
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }

    public function update(UpdateCategory $request, Category $category)
    {
        try {
            $data = $request->validated();
            $category->update($data);
            return ResponseFormatter::success(
                new CategoryResource($category),
                'Category updated successfully',
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }

    public function destroy($category)
    {
        try {
            $category = Category::findOrFail($category);
            $category->reports()->update(['category_id' => null]);
            $category->delete();
            return ResponseFormatter::success(
                null,
                'Category deleted successfully',
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }
}
