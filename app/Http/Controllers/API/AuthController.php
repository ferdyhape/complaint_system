<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Auth\LoginRequest;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        try {
            $credentials = $request->validated();
            $user = User::where('username', $credentials['username'])->first();
            if (!$user || !Hash::check($credentials['password'], $user->password)) {
                return ResponseFormatter::error(
                    'Username or password is incorrect',
                    401
                );
            }

            $token = $user->createToken('token-name')->plainTextToken;

            return ResponseFormatter::success(
                [
                    'access_token' => $token,
                    'token_type' => 'Bearer',
                    'user' => new UserResource($user),
                ],
                'Authenticated'
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                'Internal Server Error',
                500
            );
        }
    }


    public function logout(Request $request)
    {
        try {
            $request->user()->currentAccessToken()->delete();
            return ResponseFormatter::success(
                'Logout Success'
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }

    public function me(Request $request)
    {
        try {
            return ResponseFormatter::success(
                new UserResource($request->user()),
                'Data retrieved successfully'
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }
}
