<?php

namespace App\Http\Controllers\API;

use App\Models\Reporter;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Reporter\CreateReporter;
use App\Http\Requests\Reporter\UpdateReporter;
use App\Http\Resources\Reporter\ReporterResource;
use Yajra\DataTables\Facades\DataTables;

class ReporterController extends Controller
{
    public function index(Request $request)
    {
        try {
            $reporters = Reporter::all();
            return Datatables::of($reporters)
                ->addIndexColumn()

                ->make(true);
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }

    public function indexToJson()
    {
        try {
            $reporters = Reporter::all();
            return ResponseFormatter::success(
                ReporterResource::collection($reporters),
                'Reporters data retrieved successfully',
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }

    public function show(Reporter $reporter)
    {
        try {
            return ResponseFormatter::success(
                new ReporterResource($reporter),
                'Reporter data retrieved successfully'
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }

    public function store(CreateReporter $request)
    {
        try {
            $data = $request->validated();

            $reporter = Reporter::firstOrCreate([
                'identity_number' => $data['identity_number'],
                'identity_type' => $data['identity_type'],
            ], $data);

            return ResponseFormatter::success(
                new ReporterResource($reporter),
                'Reporter data created successfully'
            );
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }


    public function update(UpdateReporter $request, Reporter $reporter)
    {
        try {
            $data = $request->validated();

            $validatedReporter = Reporter::where('identity_number', $data['identity_number'])
                ->where('identity_type', $data['identity_type'])->first();

            if (
                $validatedReporter &&
                $validatedReporter->id == $reporter->id
            ) {
                $reporter->update($data);
                $reporter->save();

                return ResponseFormatter::success(
                    new ReporterResource($reporter),
                    'Reporter data updated successfully'
                );
            } else {
                return ResponseFormatter::error(
                    'Reporter with the same identity number and identity type already exists',
                    422
                );
            }
        } catch (\Exception $e) {
            return ResponseFormatter::error(
                $e->getMessage(),
                500
            );
        }
    }
}
