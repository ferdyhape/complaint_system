<?php

namespace App\Http\Requests\Reporter;

use Illuminate\Foundation\Http\FormRequest;

class UpdateReporter extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|min:3|max:255',
            'email' => 'required|email:rfc,dns|unique:reporters,email,' . $this->reporter->id,
            'phone_number' => 'required|numeric|digits_between:10,15',
            'identity_type' => 'required|string|min:2|max:255|in:KTP,SIM',
            'identity_number' => 'required|numeric|digits_between:16,20',
            'place_of_birth' => 'required|string|min:3|max:255',
            'date_of_birth' => 'required|date',
            'address' => 'required|string|min:3|max:255',
        ];
    }
}
