<?php

namespace App\Http\Requests\Reporter;

use Illuminate\Foundation\Http\FormRequest;

class CreateReporter extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|min:3|max:255',
            'email' => 'required|email:rfc,dns',
            'phone_number' => 'required|numeric|digits_between:10,15',
            'identity_type' => 'required|string|min:2|max:255|in:KTP,SIM',
            'identity_number' => 'required|numeric|digits_between:16,20',
            'place_of_birth' => 'required|string|min:3|max:255',
            'date_of_birth' => 'required|date',
            'address' => 'required|string|min:3|max:255',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nama harus diisi',
            'name.string' => 'Nama harus berupa string',
            'name.min' => 'Nama minimal 3 karakter',
            'name.max' => 'Nama maksimal 255 karakter',
            'email.required' => 'Email harus diisi',
            'email.email' => 'Email harus berupa email',
            'email.unique' => 'Email sudah terdaftar',
            'phone_number.required' => 'Nomor telepon harus diisi',
            'phone_number.numeric' => 'Nomor telepon harus berupa angka',
            'phone_number.digits_between' => 'Nomor telepon minimal 10 digit dan maksimal 15 digit',
            'identity_type.required' => 'Jenis identitas harus diisi',
            'identity_type.string' => 'Jenis identitas harus berupa string',
            'identity_type.min' => 'Jenis identitas minimal 2 karakter',
            'identity_type.max' => 'Jenis identitas maksimal 255 karakter',
            'identity_type.in' => 'Jenis identitas harus KTP atau SIM',
            'identity_number.required' => 'Nomor identitas harus diisi',
            'identity_number.numeric' => 'Nomor identitas harus berupa angka',
            'identity_number.digits_between' => 'Nomor identitas minimal 16 digit dan maksimal 20 digit',
            'place_of_birth.required' => 'Tempat lahir harus diisi',
            'place_of_birth.string' => 'Tempat lahir harus berupa string',
            'place_of_birth.min' => 'Tempat lahir minimal 3 karakter',
            'place_of_birth.max' => 'Tempat lahir maksimal 255 karakter',
            'date_of_birth.required' => 'Tanggal lahir harus diisi',
            'date_of_birth.date' => 'Tanggal lahir harus berupa tanggal',
            'address.required' => 'Alamat harus diisi',
            'address.string' => 'Alamat harus berupa string',
            'address.min' => 'Alamat minimal 3 karakter',
            'address.max' => 'Alamat maksimal 255 karakter',
        ];
    }
}
