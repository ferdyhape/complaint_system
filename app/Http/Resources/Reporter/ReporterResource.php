<?php

namespace App\Http\Resources\Reporter;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ReporterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'Name' => $this->name,
            'Identity Type' => $this->identity_type,
            'Identity Number' => $this->identity_number,
            'Place Of Birth' => $this->place_of_birth,
            'Date Of Birth' => $this->date_of_birth,
            'Address' => $this->address,
            'Phone Number' => $this->phone_number,
            'Email' => $this->email,
            'Created At' => (string) $this->created_at,
            'Updated At' => (string) $this->updated_at,
        ];
    }
}
