<?php

namespace App\Http\Resources\Report;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ReportSpecificResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $images = $this->getMedia('reports');
        $images = $images->map(function ($image) {
            return $image->getFullUrl();
        });
        $images = $images->toArray();


        if ($this->category !== null) {
            $categoryName = $this->category->name;
        } else {
            $categoryName = "Not selected";
        }


        return [
            'Id' => $this->id,
            'Reporter' => $this->reporter->name,
            'Category' => $categoryName,
            'Ticket Id' => $this->ticket_id,
            'Title' => $this->title,
            'Status' => $this->status,
            'Description' => $this->description,
            'Image' => $images,
        ];
    }
}
