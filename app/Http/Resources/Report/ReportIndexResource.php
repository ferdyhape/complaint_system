<?php

namespace App\Http\Resources\Report;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ReportIndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'reporter' => $this->reporter,
            'category' => $this->category->name ?? null,
            'ticket_id' => $this->ticket_id,
            'title' => $this->title,
            'status' => $this->status,
            'description' => Str::limit($this->description, 50),
        ];
    }
}
