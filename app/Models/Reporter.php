<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reporter extends Model
{
    use HasFactory;

    protected  $fillable = [
        'name',
        'email',
        'phone_number',
        'identity_number',
        'identity_type',
        'place_of_birth',
        'date_of_birth',
        'address'
    ];

    public function reports()
    {
        return $this->hasMany(Report::class);
    }
}
