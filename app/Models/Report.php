<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Report extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia, LogsActivity;

    protected  $fillable = [
        'reporter_id',
        'category_id',
        'ticket_id',
        'title',
        'status',
        'description'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($report) {
            $now = now();
            $formattedDate = $now->format('Ymd');
            $countOfReport = Report::whereDate('created_at', $now)->count() + 1;
            $reportNumber = str_pad($countOfReport, 5, '0', STR_PAD_LEFT);
            $report->ticket_id = $formattedDate . $reportNumber;
        });
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['title', 'status', 'description', 'reporter_id', 'category_id', 'ticket_id'])
            ->setDescriptionForEvent(fn (string $eventName) => "This model has been {$eventName}")
            ->useLogName('Report');
    }

    public function reporter()
    {
        return $this->belongsTo(Reporter::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function reportTrackers()
    {
        return $this->hasMany(ReportTracker::class);
    }
}
