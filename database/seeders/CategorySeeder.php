<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    public function run(): void
    {
        $categoryNames = [
            'Infrastruktur',
            'Lingkungan',
            'Layanan Publik',
            'Keamanan',
            'Kesehatan',
            'Lain - Lain'
        ];

        foreach ($categoryNames as $name) {
            Category::create([
                'name' => $name,
                'slug' => strtolower($name)
            ]);
        }
    }
}
