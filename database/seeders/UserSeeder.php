<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'name' => 'Ferdy Hahan Pradana',
            'email' => 'ferdy@gmail.com',
            'username' => 'ferdyhape',
            'phone_number' => '087856725286',
            'password' => bcrypt('password'),
        ]);

        User::factory()
            ->count(5)
            ->create();
    }
}
