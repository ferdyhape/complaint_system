import {
    createRouter,
    createWebHistory
} from "vue-router";


import Home from "@/Pages/Home.vue";
import Report from "@/Pages/Report.vue";

import Login from "@/Pages/Auth/Login.vue";
import Category from "@/Pages/Auth/Category.vue";
import Reporter from "@/Pages/Auth/Reporter.vue";
import ReportForAdmin from "@/Pages/Auth/Report.vue";
import ReportTracker from "@/Pages/Auth/ReportTracker.vue";
import ReporterDetail from "@/Pages/Auth/ReporterDetail.vue";


const routes = [{
        path: "/",
        name: "Home",
        component: Home
    },
    {
        path: "/login",
        name: "Login",
        component: Login
    },
    {
        path: "/report",
        name: "Report",
        component: Report
    },
    {
        path: "/dashboard/categories",
        name: "Category",
        component: Category
    },
    {
        path: "/dashboard/reporters",
        name: "Reporter",
        component: Reporter
    },
    {
        path: "/dashboard/reports",
        name: "ReportForAdmin",
        component: ReportForAdmin
    },
    {
        path: "/dashboard/report-tracker",
        name: "ReportTracker",
        component: ReportTracker
    },
    {
        path: "/dashboard/reporters/:id",
        name: "ReporterDetail",
        component: ReporterDetail,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;
