import {
    createApp
} from 'vue';
import App from './App.vue';
import router from './Router/index.js';
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";

window.apiUrl = document.getElementById('app').getAttribute('data-api-url');

const VueApp = createApp(App);
VueApp.config.globalProperties.$apiUrl = window.apiUrl;
VueApp.use(router);
VueApp.mount('#app');
